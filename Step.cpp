#include <Arduino.h>
#include <string>

#include "Step.h"

StepVoice::StepVoice(int statusParameter, float speedParameter)
{
  st = statusParameter;
  sp = speedParameter;
}

std::string StepVoice::toJsonString() {
  return "{\"st\":" + std::to_string(st) + ",\"sp\":" + std::to_string(sp) + "}";
  //return "{\"st\":0,\"sp\": 1.0}";
}

Step::Step(int voicesNumberParameter)
{
  sV.resize(voicesNumberParameter, StepVoice());
}

std::string Step::toJsonString() {
  std::string concatenatedStepVoices;
  for (size_t i = 0; i < sV.size(); ++i) {
    if (i != 0) {
      concatenatedStepVoices += ", ";
    }
    //concatenatedStepVoices += "\"";
    concatenatedStepVoices += sV[i].toJsonString();
    //concatenatedStepVoices += "\"";
  }
  
  return "{\"sV\": [" + concatenatedStepVoices + "]}";
}
