/*********
  IDKWID
*********/
#include <iostream>
#include <string>

#include "XT_DAC_Audio.h"
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

#include "WebApp.h"
#include "Utils.h"
#include "SoundData.h"
#include "MachineState.h"
#include "Part.h"
#include "Track.h"
#include "Step.h"

// const char* ssid = "Tardigrad";
// const char* password = "blblblbl";
const char *ssid = "SFR_25AF";
const char *password = "pk88u26tgpg1k48xrue2";
// const char* ssid = "Backstage-WiFi";
// const char* password = "44VWSvwMpXXqy3b0GXedTi5R1LtGVnYS";

AsyncWebServer server(80);
AsyncWebSocket ws("/ws");

MachineState *machineState;

bool isPlaying = false;
int currentStepIndex = 0;
unsigned long previousTime = 0;

XT_DAC_Audio_Class DacAudio(25,0);    // Create the main player class object. 
                                      // Use GPIO 25, one of the 2 DAC pins and timer 0
XT_Wav_Class KickSample(Kick);
XT_Wav_Class SnareSample(Snare);
XT_Wav_Class HitHatSample(HitHat);
XT_Wav_Class BassSample(Bass);

// std::string machineStateString = "Setting up...";

void setup()
{
  Serial.begin(115200);
  
  Serial.println("----------------");
  Serial.println("----------------");
  Serial.println("SetUp Start");
  Serial.println("................");

  initMachine();
  /*
  pinMode(21, INPUT);
  pinMode(22, INPUT);
  pinMode(23, INPUT);
  */

  initSequencerThread();

  initRGBLeds();

  initWifi();

  Serial.println("................");
  Serial.println("SetUp Done");
  Serial.println("----------------");
  Serial.println("----------------");
}

void initMachine()
{
  machineState = new MachineState();
  //machineStateString = machineState->toJsonString();
  Serial.println("Machine State Initialized");
  //Serial.println(machineStateString);
}

void initSequencerThread() {

}

void initRGBLeds() {

  std::string hexColor = machineState->machineConfig.lColor;
  
  int r = hexStringToInt(hexColor.substr(1, 2)); // Extracts "FF" and converts to int
  r = int(r * machineState->machineConfig.lIntensity); // Use led intensity config
  int g = hexStringToInt(hexColor.substr(3, 2)); // Extracts "FF" and converts to int
  g = int(g * machineState->machineConfig.lIntensity); // Use led intensity config
  int b = hexStringToInt(hexColor.substr(5, 2)); // Extracts "00" and converts to int
  b = int(b * machineState->machineConfig.lIntensity); // Use led intensity config
  
#ifdef RGB_BUILTIN
  digitalWrite(RGB_BUILTIN, HIGH);   // Turn the RGB LED white
  delay(1000);
  digitalWrite(RGB_BUILTIN, LOW);    // Turn the RGB LED off
  delay(1000);
  neopixelWrite(RGB_BUILTIN,r,g,b);
#endif
}

void initWifi()
{
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  initWebSocket();

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send_P(200, "text/html", index_html, processor); });

  // Start server
  server.begin();

  Serial.println("HTTP server started");
}

void initWebSocket()
{
  ws.onEvent(onEvent);
  server.addHandler(&ws);
}

void loop()
{
  ws.cleanupClients();

  if(isPlaying) {
    DacAudio.FillBuffer();
    sequencerThreadFunction();
  }

  /*
  if(digitalRead(23)) {
    Serial.println("Play or Stop");
    isPlaying = !isPlaying;
    delay(500);
  }
  if(digitalRead(21)) {
    Serial.println("Prev Part");
    int newSelectedPartId = machineState->selectedPartId - 1;
    
    machineState->selectedPartId = scrollListIndexes(newSelectedPartId, machineState->parts.size());
    //machineStateString = machineState->toJsonString();
    notifyClients();
    delay(500);
  }
  if(digitalRead(22)) {
    Serial.println("Next Part");
    int newSelectedPartId = machineState->selectedPartId + 1;
    
    machineState->selectedPartId = scrollListIndexes(newSelectedPartId, machineState->parts.size());
    //machineStateString = machineState->toJsonString();
    notifyClients();
    delay(500);
  }
  */
}

void sequencerThreadFunction() {
  int stepsLength = machineState->parts[machineState->selectedPartId].stepsLength;
  int signature= machineState->parts[machineState->selectedPartId].signature;
  int tempo = machineState->parts[machineState->selectedPartId].tempo;
  std::vector<Step> steps = machineState->parts[machineState->selectedPartId].drumRack.steps;
  int second = 1000;
  int minute = 60 * second;
    
  unsigned long currentTime = millis();
  // number of beat per minutes / number of steps per beat (16 is the bar resolution resolution => number of steps in a bar)
  int delayInMillis = (minute/tempo) / (16/signature);

  // Task 1: Run every interval1 milliseconds
  if (currentTime - previousTime >= delayInMillis) {
    previousTime = currentTime;

    Step step = steps[currentStepIndex];
    currentStepIndex += 1;
    if(currentStepIndex >= stepsLength) {
      currentStepIndex = 0;
    }
    
    for(size_t i = 0; i < step.sV.size(); i += 1) {
      if(step.sV[i].st && i == 0) { 
        //Serial.println("Kick");
        KickSample.Speed = step.sV[i].sp;
        DacAudio.Play(&KickSample);
      } else if(step.sV[i].st && i == 1) {
        //Serial.println("Snare");
        SnareSample.Speed = step.sV[i].sp;
        DacAudio.Play(&SnareSample);
      } else if(step.sV[i].st && i == 2) {
        //Serial.println("HitHat");
        BassSample.Speed = step.sV[i].sp;
        DacAudio.Play(&HitHatSample);
      } else if(step.sV[i].st && i == 3) {
        //Serial.println("Bass");
        BassSample.Speed = step.sV[i].sp;
        DacAudio.Play(&BassSample);
      }  
    }
    
  }

}

void handleWebSocketMessage(void *arg, uint8_t *data, size_t len)
{
  AwsFrameInfo *info = (AwsFrameInfo *)arg;
  const char *dataAsChars = (char *)data;
  Serial.printf("dataAsChars : %s\n", dataAsChars);
  if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT)
  {
    data[len] = 0;
    
    if (strncmp(dataAsChars, "upst", 4) == 0)
    {
      Serial.println("Update state");
      size_t inputLength = strlen(dataAsChars);
      char* newStateString = new char[inputLength - 4 + 1]; // +1 for the null terminator
      strcpy(newStateString, dataAsChars + 4);

      //std::string cppString(newStateString); // Convert char* to std::string
    
      machineState->updateState(std::string(newStateString));
      //machineStateString = machineState->toJsonString();
      //Serial.println("AFTERAFTER :");
      //Serial.println(machineState->parts[machineState->selectedPartId].drumRack.steps[0].sV[0]);
      Serial.println("machineState->toJsonString() : ");
      Serial.println(machineState->toJsonString().c_str());
      notifyClients();
    }
    else if (strncmp(dataAsChars, "play", 4) == 0)
    {
      Serial.println("Play");
      isPlaying = true;
    }
    else if (strncmp(dataAsChars, "stop", 4) == 0)
    {
      Serial.println("Stop");
      isPlaying = false;
    }
    else if (strncmp(dataAsChars, "nxpt", 4)) {
      Serial.println("Next Part");
      int newSelectedPartId = machineState->selectedPartId + 1;
      
      machineState->selectedPartId = scrollListIndexes(newSelectedPartId, machineState->parts.size());
      //machineStateString = machineState->toJsonString();
      notifyClients();
    }
    else if (strncmp(dataAsChars, "prpt", 4)) {
      Serial.println("Prev Part");
      int newSelectedPartId = machineState->selectedPartId - 1;
      
      machineState->selectedPartId = scrollListIndexes(newSelectedPartId, machineState->parts.size());
      //machineStateString = machineState->toJsonString();
      notifyClients();
    }
  }
}

void notifyClients()
{
  Serial.println("notifyCLient");
  String state;
  state.concat(machineState->toJsonString().c_str());
  ws.textAll(state);
  //ws.textAll(machineStateString.c_str());
}

String processor(const String &var)
{
  //Serial.println(var);
  if (var == "MACHINE_STATE")
  {
    String result;
    result.concat(machineState->toJsonString().c_str());
    return result;
  }
  return String();
}

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len)
{
  switch (type)
  {
  case WS_EVT_CONNECT:
    Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
    break;
  case WS_EVT_DISCONNECT:
    Serial.printf("WebSocket client #%u disconnected\n", client->id());
    break;
  case WS_EVT_DATA:
    // Serial.printf("Toc\n");
    handleWebSocketMessage(arg, data, len);
    break;
  case WS_EVT_PONG:
  case WS_EVT_ERROR:
    break;
  }
}
