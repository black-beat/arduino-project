#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>

#include "Enums.h"

// Function to convert hex string to integer
inline int hexStringToInt(const std::string& hexStr) {
    int value;
    std::istringstream(hexStr.substr(1)) >> std::hex >> value;

    return value;
}

inline StepsLength mapStepsLength(int length) {
    switch (length) {
        case 8: return STEPSLENGTH_8;
        case 16: return STEPSLENGTH_16;
        case 32: return STEPSLENGTH_32;
        case 64: return STEPSLENGTH_64;
        default: return STEPSLENGTH_16; // Default to a reasonable value
    }
}

inline int scrollListIndexes(int desiredInt, int listLength) {
  if(desiredInt < 0) {
    return listLength - 1;
  } else if(desiredInt >= listLength) {
    return 0;
  }
  return desiredInt;
}

#endif // UTILS_H
