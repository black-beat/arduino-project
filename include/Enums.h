#ifndef ENUMS_H
#define ENUMS_H

enum StepsLength {
    STEPSLENGTH_8 = 8,
    STEPSLENGTH_16 = 16,
    STEPSLENGTH_32 = 32,
    STEPSLENGTH_64 = 64
};

#endif // ENUMS_H
