#ifndef MACHINECONFIG_H
#define MACHINECONFIG_H

class MachineConfig {
public:
    MachineConfig();

    float lIntensity;
    std::string lColors[7];
    std::string lColor;
    float volume;

    std::string toJsonString();

private:
    //std::string lightcolor;
    //double volume;
};
#endif // MACHINECONFIG_H
