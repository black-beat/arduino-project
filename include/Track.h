#ifndef TRACK_H
#define TRACK_H

#include <iostream>
#include <vector>
#include <string>

#include "Enums.h"
#include "Step.h"

class Track {
public:
    Track(StepsLength stepsLengthParameter = STEPSLENGTH_16, int voicesNumberParameter = 9);
    int voicesNumber;
    std::vector<float> voicesVolume;
    
    std::vector<Step> steps;

    std::string toJsonString();
    void resize(StepsLength stepsLength);

private:
};
#endif // TRACK_H
