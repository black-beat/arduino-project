#ifndef GLOABALVARS_H
#define GLOABALVARS_H

#include "MachineState.h"

inline MachineState *machineState;
inline bool isPlaying = false;
inline int currentStepIndex = 0;
inline unsigned long previousTime = 0;

#endif // GLOABALVARS_H
