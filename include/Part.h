#ifndef PART_H
#define PART_H

#include "Enums.h"
#include "Track.h"

class Part {
public:
    //Part(int tempoParameter = 120, int signatureParameter = 4, StepsLength stepsLengthParameter = STEPSLENGTH_16);
    //Part(int tempoParameter = 90, int signatureParameter = 4, StepsLength stepsLengthParameter = STEPSLENGTH_16);
    Part(int tempoParameter = 165, int signatureParameter = 4, StepsLength stepsLengthParameter = STEPSLENGTH_16, int voicesNumberParameter = 9);

    int tempo;
    int signature;
    StepsLength stepsLength;
    int voicesNumber;
    Track drumRack;
    //std::vector<std::string>;

    std::string toJsonString();
    void resize(StepsLength stepsLength);
private:
};
#endif // PART_H
