#ifndef INIT_H
#define INIT_H

#include <iostream>
#include <stdio.h>
#include <string>

#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

#include "WebApp.h"
#include "GlobalVars.h"
#include "Enums.h"
#include "Utils.h"
// #include "SoundData.h"
#include "MachineState.h"
// #include "Part.h"
// #include "Track.h"
// #include "Step.h"

// #define EXAMPLE_ESP_WIFI_SSID "SFR_25AF"
// #define EXAMPLE_ESP_WIFI_PASS "pk88u26tgpg1k48xrue2"
// #define EXAMPLE_MAX_STA_CONN (10)
// static const char *TAG = "wifi softAP";
// const char* ssid = "Tardigrad";
// const char* password = "blblblbl";
const char *ssid = "SFR_25AF";
const char *password = "pk88u26tgpg1k48xrue2";
// const char* ssid = "Backstage-WiFi";
// const char* password = "44VWSvwMpXXqy3b0GXedTi5R1LtGVnYS";

AsyncWebServer server(80);
AsyncWebSocket ws("/ws");

// void initMachineState(void *null)
// void initMachineState(MachineState *machineState)
void initMachineState()
{
    // while (1)
    // {
    //     vTaskDelay(1000 / portTICK_PERIOD_MS);
    // }

    machineState = new MachineState();
    // machineStateString = machineState->toJsonString();
    printf("Machine State Initialized\n");
    // printf(machineStateString);

    // vTaskDelete(NULL);
}

String processor(const String &var)
{
    // printf(var);
    if (var == "MACHINE_STATE")
    {
        String result;
        result.concat(machineState->toJsonString().c_str());
        return result;
    }
    return String();
}

void notifyClients()
{
    printf("notifyCLient\n");
    String state;
    state.concat(machineState->toJsonString().c_str());
    ws.textAll(state);
    // ws.textAll(machineStateString.c_str());
}

// void handleWebSocketMessage(void *arg, uint8_t *data, size_t len, bool *isPlaying, int *currentStepIndex)
void handleWebSocketMessage(void *arg, uint8_t *data, size_t len)
{
    AwsFrameInfo *info = (AwsFrameInfo *)arg;
    const char *dataAsChars = (char *)data;
    printf("dataAsChars : %s\n", dataAsChars);
    if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT)
    {
        data[len] = 0;

        if (strncmp(dataAsChars, "upst", 4) == 0)
        {
            printf("Update state\n");
            size_t inputLength = strlen(dataAsChars);
            char *newStateString = new char[inputLength - 4 + 1]; // +1 for the null terminator
            strcpy(newStateString, dataAsChars + 4);

            // std::string cppString(newStateString); // Convert char* to std::string

            machineState->updateState(std::string(newStateString));
            // machineStateString = machineState->toJsonString();
            // printf("AFTERAFTER :");
            // printf(machineState->parts[machineState->selectedPartId].drumRack.steps[0].sV[0]);
            printf("machineState->toJsonString() : \n");
            printf(machineState->toJsonString().c_str());
            printf("\n");
            notifyClients();
        }
        else if (strncmp(dataAsChars, "play", 4) == 0)
        {
            printf("Play\n");
            isPlaying = true;
        }
        else if (strncmp(dataAsChars, "stop", 4) == 0)
        {
            printf("Stop\n");
            isPlaying = false;
        }
        else if (strncmp(dataAsChars, "nxpt", 4))
        {
            printf("Next Part\n");
            int newSelectedPartId = machineState->selectedPartId + 1;

            machineState->selectedPartId = scrollListIndexes(newSelectedPartId, machineState->parts.size());
            // machineStateString = machineState->toJsonString();
            notifyClients();
        }
        else if (strncmp(dataAsChars, "prpt", 4))
        {
            printf("Prev Part\n");
            int newSelectedPartId = machineState->selectedPartId - 1;

            machineState->selectedPartId = scrollListIndexes(newSelectedPartId, machineState->parts.size());
            // machineStateString = machineState->toJsonString();
            notifyClients();
        }
    }
}

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len)
{
    switch (type)
    {
    case WS_EVT_CONNECT:
        printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
        printf("\n");
        break;
    case WS_EVT_DISCONNECT:
        printf("WebSocket client #%u disconnected\n", client->id());
        printf("\n");
        break;
    case WS_EVT_DATA:
        // printf("Toc\n");
        handleWebSocketMessage(arg, data, len);
        break;
    case WS_EVT_PONG:
    case WS_EVT_ERROR:
        break;
    }
}

// void initWebServer(void *null)
// void initWebServer(MachineState *machineState, bool *isPlaying, int *currentStepIndex)
// void initWebServer(MachineState *machineState)
void initWebServer()
{

    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    printf("\n");

    // Wait for connection
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        // vTaskDelay(500 / portTICK_PERIOD_MS);
        printf(".");
    }
    printf("\n");
    printf("Connected to : %s / Ip address : %s", ssid, WiFi.localIP().toString().c_str());
    printf("\n");

    ws.onEvent(onEvent);
    server.addHandler(&ws);

    // Route for root / web page
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send_P(200, "text/html", index_html, processor); });

    // Start server
    server.begin();

    printf("HTTP server started\n");

    // while (1)
    // {
    //     vTaskDelay(1000 / portTICK_PERIOD_MS);
    // }
    // vTaskDelete(NULL);
}

// void initRGBLeds(MachineState *machineState)
void initRGBLeds()
{
    std::string hexColor = machineState->machineConfig.lColor;

    int r = hexStringToInt(hexColor.substr(1, 2));       // Extracts "FF" and converts to int
    r = int(r * machineState->machineConfig.lIntensity); // Use led intensity config
    int g = hexStringToInt(hexColor.substr(3, 2));       // Extracts "FF" and converts to int
    g = int(g * machineState->machineConfig.lIntensity); // Use led intensity config
    int b = hexStringToInt(hexColor.substr(5, 2));       // Extracts "00" and converts to int
    b = int(b * machineState->machineConfig.lIntensity); // Use led intensity config

#ifdef RGB_BUILTIN
    digitalWrite(RGB_BUILTIN, HIGH); // Turn the RGB LED white
    delay(1000);
    digitalWrite(RGB_BUILTIN, LOW); // Turn the RGB LED off
    delay(1000);
    neopixelWrite(RGB_BUILTIN, r, g, b);
#endif
}

#endif // INIT_H
