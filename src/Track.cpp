#include <string>

#include "Track.h"
#include "Enums.h"

Track::Track(StepsLength stepsLengthParameter, int voicesNumberParameter)
{
  voicesNumber = voicesNumberParameter;
  
  voicesVolume.resize(voicesNumberParameter, 1.0);
  
  //steps.resize(static_cast<int>(length));
  steps.resize((int)stepsLengthParameter, Step(voicesNumberParameter));

  // Replace odd-index elements with a different value
  /*for (size_t i = 0; i < steps.size(); i += 8) {
      steps[i] = "0";
  }
  for (size_t i = 4; i < steps.size(); i += 8) {
      steps[i] = "1";
  }
  for (size_t i = 0; i < steps.size(); i += 2) {
      if(steps[i] == "-1") {
        steps[i] = "2";
      }
  }*/
  /*steps[0] = "0";
  steps[2] = "2";
  steps[4] = "1";
  steps[6] = "2";
  steps[8] = "2";
  steps[10] = "0";
  steps[12] = "1";
  steps[14] = "2";
  steps[15] = "2";*/
}

std::string Track::toJsonString() {
  std::string concatenedVoicesVolume;
  for (size_t i = 0; i < voicesVolume.size(); ++i) {
    if (i != 0) {
      concatenedVoicesVolume += ", ";
    }
    //concatenedVoicesVolume += "\"";
    concatenedVoicesVolume += std::to_string(voicesVolume[i]);
    //concatenedVoicesVolume += "\"";
  }
  
  std::string concatenatedSteps;
  //for (const auto& step : steps) {
  for (size_t i = 0; i < steps.size(); ++i) {
    if (i != 0) {
      concatenatedSteps += ", ";
    }
    //concatenatedSteps += "\"";
    concatenatedSteps += steps[i].toJsonString();
    //concatenatedSteps += "\"";
  }
  
  return "{\"voicesVolume\":[" + concatenedVoicesVolume + "],\"steps\":[" + concatenatedSteps + "]}";
  //return "{\"steps\": []}";
}

void Track::resize(StepsLength stepsLength) {
  //steps.resize(static_cast<int>(length));
  steps.resize((int)stepsLength, Step(voicesNumber));
}
