/*********
  IDKWID
*********/
#include <iostream>
#include <string>

#include <stdio.h>
// #include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include "freertos/task.h"
// #include "freertos/event_groups.h"
// #include "esp_wifi.h"
// #include "esp_log.h"
// #include "esp_event.h"
// #include "nvs_flash.h"

// #include <Arduino.h>
// #include <WiFi.h>
// #include <AsyncTCP.h>
// #include <ESPAsyncWebSrv.h>
#include "XT_DAC_Audio.h"
// #include <TFT_eSPI.h> // Include the graphics library

#include "Init.h"
#include "WebApp.h"
#include "GlobalVars.h"
#include "Enums.h"
#include "Utils.h"
#include "SoundData.h"
#include "MachineState.h"
// #include "Part.h"
// #include "Track.h"
// #include "Step.h"

// extern "C"
// {

XT_DAC_Audio_Class DacAudio(25, 0); // Create the main player class object.
                                    // Use GPIO 25, one of the 2 DAC pins and timer 0
XT_Wav_Class KickSample(Kick);
XT_Wav_Class SnareSample(Snare);
XT_Wav_Class HitHatSample(HitHat);
XT_Wav_Class BassSample(Bass);

// TFT_eSPI tft = TFT_eSPI(); // Create object "tft"
// static const char *TAG = "MAIN";

void sequencerThreadFunction()
{
    int stepsLength = machineState->parts[machineState->selectedPartId].stepsLength;
    int signature = machineState->parts[machineState->selectedPartId].signature;
    int tempo = machineState->parts[machineState->selectedPartId].tempo;
    std::vector<Step> steps = machineState->parts[machineState->selectedPartId].drumRack.steps;
    int second = 1000;
    int minute = 60 * second;

    unsigned long currentTime = millis();
    // number of beat per minutes / number of steps per beat (16 is the bar resolution resolution => number of steps in a bar)
    int delayInMillis = (minute / tempo) / (16 / signature);

    // Task 1: Run every interval1 milliseconds
    if (currentTime - previousTime >= delayInMillis)
    {
        previousTime = currentTime;

        Step step = steps[currentStepIndex];
        currentStepIndex += 1;
        if (currentStepIndex >= stepsLength)
        {
            currentStepIndex = 0;
        }

        for (size_t i = 0; i < step.sV.size(); i += 1)
        {
            if (step.sV[i].st && i == 0)
            {
                // Serial.println("Kick");
                KickSample.Speed = step.sV[i].sp;
                DacAudio.Play(&KickSample);
            }
            else if (step.sV[i].st && i == 1)
            {
                // Serial.println("Snare");
                SnareSample.Speed = step.sV[i].sp;
                DacAudio.Play(&SnareSample);
            }
            else if (step.sV[i].st && i == 2)
            {
                // Serial.println("HitHat");
                BassSample.Speed = step.sV[i].sp;
                DacAudio.Play(&HitHatSample);
            }
            else if (step.sV[i].st && i == 3)
            {
                // Serial.println("Bass");
                BassSample.Speed = step.sV[i].sp;
                DacAudio.Play(&BassSample);
            }
        }
    }
}

void audioTask(void *null)
{
    // printf("AudioTask :\n");
    while (1)
    {
        ws.cleanupClients();

        if (isPlaying)
        {
            DacAudio.FillBuffer();
            sequencerThreadFunction();
        }
        // printf("audioTask\n");
        // vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    vTaskDelete(NULL);
}

// void inputsTask(void *null)
// {
//     while (1)
//     {
//         vTaskDelay(1000 / portTICK_PERIOD_MS);
//     }
//     vTaskDelete(NULL);
// }

// void screenDisplayTask(void *null)
// {
//     while (1)
//     {
//         vTaskDelay(1000 / portTICK_PERIOD_MS);
//     }
//     vTaskDelete(NULL);
// }

void setup()
{
    printf("This is Black Beat\n");

    // initRGBLeds();

    initMachineState();
    initWebServer();

    printf("This is Black Beat\n");

    xTaskCreate(audioTask, "audioTask", 1024, NULL, 5, NULL);
    // xTaskCreate(inputsTask, "inputsTask", 512, NULL, 3, NULL);
    // xTaskCreate(screenDisplayTask, "screenDisplayTask", 512, NULL, 1, NULL);
}

void loop()
{
    // printf("Again!\n");
    // esp_rom_delay_us(1000000);
    // loop();
}

// void app_main(void)
// {
//     printf("WTF\n");
//     printf("WTF\n");
//     printf("WTF\n");
//     printf("WTF\n");
//     printf("WTF\n");

//     // my_setup();

//     // while (1)
//     // {
//     // my_loop();
//     // esp_rom_delay_us(1000000);
//     // vTaskDelay(1000 / portTICK_PERIOD_MS);
//     // }
// }

// }