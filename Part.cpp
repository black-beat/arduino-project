#include <Arduino.h>
#include <string>

#include "Part.h"
#include "Enums.h"
#include "Track.h"

// Part::Part(Lengths length): length(length)
Part::Part(int tempoParameter, int signatureParameter, StepsLength stepsLengthParameter, int voicesNumberParameter)
{
    tempo = tempoParameter;
    // stepsLength = STEPSLENGTH_8;
    signature = signatureParameter;
    stepsLength = stepsLengthParameter;
    voicesNumber = voicesNumberParameter;
    drumRack = Track(stepsLengthParameter, voicesNumberParameter);
}

std::string Part::toJsonString() {
  return "{\"tempo\":" + std::to_string(tempo) + ",\"signature\":" + std::to_string(signature) + ",\"stepsLength\":" + std::to_string((int)stepsLength) + ",\"voicesNumber\":" + std::to_string(voicesNumber) + ",\"drumRack\":" + drumRack.toJsonString() + "}";
}

void Part::resize(StepsLength stepsLength) {
  //steps.resize(static_cast<int>(length));
  //steps.resize((int)stepsLength);
  drumRack.resize(stepsLength);
}
