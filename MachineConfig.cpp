#include <Arduino.h>
#include <string>

#include "MachineConfig.h"

// Constructor implementation for MachineConfig
// MachineConfig::MachineConfig() : lightcolor("red"), volume(1.0)
MachineConfig::MachineConfig()
{
    lIntensity = 0.25;
    lColors[0] = "#FF0000";
    lColors[1] = "#FF7F00";
    lColors[2] = "#FFFF00";
    lColors[3] = "#00FF00";
    lColors[4] = "#007FFF";
    lColors[5] = "#0000FF";
    lColors[6] = "#7F00FF";
    lColor = lColors[2];
    volume = 0.5;
}

std::string MachineConfig::toJsonString() {
  return "{\"lIntensity\":" + std::to_string(lIntensity) + ",\"lColor\":\"" + lColor + "\",\"volume\":" + std::to_string(volume) + "}";
}
