#include <Arduino.h>
#include <string>
#include <ArduinoJson.h>
#include <vector>

#include "MachineState.h"
#include "MachineConfig.h"
#include "Part.h"
#include "Utils.h"

MachineState::MachineState()
// MachineState::MachineState(const std::string &description)
//  : description(description), machineConfig()
{
    machineConfig = MachineConfig();
    selectedPartId = 0;
    // default params :
    // parts.resize(3, Part(165, 4, STEPSLENGTH_16, 9));
    // parts.resize(3, Part(165, 4, STEPSLENGTH_32, 2));
    parts.resize(3, Part(112, 4, STEPSLENGTH_16, 4));
    //steps[0] = Part();
}

void MachineState::updateState(std::string newState) {

  Serial.println(ESP.getFreeHeap());
  DynamicJsonDocument root(4096); // Adjust the size as needed
  DeserializationError error = deserializeJson(root, newState);
  if (error) {
      Serial.println("Failed to parse JSON");
      Serial.println(error.c_str());
      return;
  }
    
  machineConfig.lIntensity = root["machineConfig"]["lIntensity"];
  std::string lColor = root["machineConfig"]["lColor"];
  machineConfig.lColor = lColor;
  machineConfig.volume = root["machineConfig"]["volume"];
  parts[selectedPartId].tempo = root["selectedPart"]["tempo"];
  parts[selectedPartId].signature = root["selectedPart"]["signature"];
  parts[selectedPartId].stepsLength = mapStepsLength(root["selectedPart"]["stepsLength"]);

  for (int voicesVolumeIndex = 0; voicesVolumeIndex < parts[selectedPartId].drumRack.voicesVolume.size(); ++voicesVolumeIndex) {
    parts[selectedPartId].drumRack.voicesVolume[voicesVolumeIndex] = root["selectedPart"]["drumRack"]["voicesVolume"][voicesVolumeIndex].as<float>();
  }

  for (int stepIndex = 0; stepIndex < parts[selectedPartId].drumRack.steps.size(); ++stepIndex) {
    for (int sVIndex = 0; sVIndex < parts[selectedPartId].drumRack.steps[stepIndex].sV.size(); ++sVIndex) {
      parts[selectedPartId].drumRack.steps[stepIndex].sV[sVIndex].st = root["selectedPart"]["drumRack"]["steps"][stepIndex]["sV"][sVIndex]["st"].as<int>();
      parts[selectedPartId].drumRack.steps[stepIndex].sV[sVIndex].sp = root["selectedPart"]["drumRack"]["steps"][stepIndex]["sV"][sVIndex]["sp"].as<float>();
    }
  }

}

std::string MachineState::toJsonString() {
    std::string concatenedParts;
  //for (const auto& step : steps) {
  for (size_t i = 0; i < parts.size(); ++i) {
    if (i != 0) {
      concatenedParts += ", ";
    }
    concatenedParts += parts[i].toJsonString();
  }
  
  return "{\"machineConfig\":" + machineConfig.toJsonString() + ", \"selectedPartId\":" + std::to_string(selectedPartId) + ", \"selectedPart\":" + parts[selectedPartId].toJsonString() + "}";
  //return "{\"machineConfig\":" + machineConfig.toJsonString() + ", \"selectedPartId\":" + std::to_string(selectedPartId) + ", \"parts\": [" + concatenedParts + "]}";
}
