#ifndef WEBAPP_H
#define WEBAPP_H

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>Black Beat Web Server</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,">
  <style>
  html {
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;
  }
  body {
    margin: 0;
  }
  .button {
  }
  .checkbox {
    width: 50px;
    height: 50px;
    //appearance: none;  /* Remove default appearance */
    //-webkit-appearance: none;
    //-moz-appearance: none;
    border: 1px solid #ccc;  /* Border to simulate a checkbox */
    outline: none;  /* Remove outline on focus */
    cursor: pointer;  /* Change cursor to pointer on hover */
  }
  .number-input {
    width: 50px;
  }
  .steps-row {
    display: flex;
    flex-direction: row;
    justify-content: center;
  }
  .step-container {
    display: flex;
    flex-direction: column;
  }
  </style>
</head>
<body>
  <div class="topnav">
    <h1>Black Beat</h1>
  </div>
  <div class="content">
    <div class="card">
      <!--
      <h2>MACHINE_STATE</h2>
      <p class="machine-state">%MACHINE_STATE%</p>
      -->
      <p>
        <label for="tempo">Tempo</label>
        <input type="range" id="tempo-slider" name="tempo" min="60" max="220" step="1" />
        <span id="tempo-slider-value"></span>
      </p>
      <p>
        <button id="save-state-button" class="button">Save state</button>
        <button id="load-state-button" class="button">Load state</button>
        <br>
        <textarea id="state-textarea" name="messageField" rows="6" cols="40"></textarea>
      </p>
      <p>
        <button id="next-part-button" class="button">Previous part</button>
        <span id="part-id-span">0</span>
        <button id="prev-part-button" class="button">Next part</button>
      </p>
      <p>
        <button id="play-button" class="button">Play</button>
        <button id="stop-button" class="button">Stop</button>
      </p>
      <p id="steps-container"></p>
    </div>
  </div>
<script>
  var gateway = `ws://${window.location.hostname}/ws`;
  var websocket;

  var initialState = %MACHINE_STATE%;
  console.log('initial state: ', initialState);
  
  window.addEventListener('load', onLoad);/*
  */function initWebSocket() {
    console.log('Trying to open a WebSocket connection...');
    websocket = new WebSocket(gateway);
    websocket.onopen    = onOpen;
    websocket.onclose   = onClose;
    websocket.onmessage = onMessage;
  }
  function initButtons() {
    //saveTextToFile
    document.getElementById('save-state-button').addEventListener('click', () => saveTextToFile(JSON.stringify(initialState), "BB-machineState.json"));
    document.getElementById('play-button').addEventListener('click', play);
    document.getElementById('stop-button').addEventListener('click', stop);
    document.getElementById('prev-part-button').addEventListener('click', prevPart);
    document.getElementById('next-part-button').addEventListener('click', nextPart);
    var tempoSliderElement = document.getElementById('tempo-slider');
    tempoSliderElement.value= initialState.selectedPart.tempo;
    tempoSliderElement.addEventListener('change', tempoChange);
    var tempoSliderValueElement = document.getElementById('tempo-slider-value');
    tempoSliderValueElement.textContent = tempoSliderElement.value;
    
  }
  function initCheckboxes() {
    //var numberOfVoices = 9;
    var numberOfVoices = initialState.selectedPart.voicesNumber;
    var numberOfStepsPerRow = 16;
    //var numberOfStepsPerRow= initialState.selectedPart.stepsLength;
    var numberOfRows = Math.ceil(initialState.selectedPart.stepsLength / 16);
    console.log("number of rows : ", numberOfRows);
    
    var stepsContainer = document.getElementById(`steps-container`);
    // Clear the content of the steps container
    stepsContainer.innerHTML = "";

    for (var voiceIndex = numberOfVoices - 1; voiceIndex >= 0; voiceIndex--) {
      var voice = document.createElement("p");
      var voiceLabel = document.createElement("b");
      voiceLabel.innerText = `Voice ${voiceIndex + 1} :`;
      
      voice.appendChild(voiceLabel);
      stepsContainer.appendChild(voice);

      var voiceVolume = document.createElement("input");
      voiceVolume.type = "range";
      voiceVolume.id = `voice-volume-${voiceIndex}`;
      voiceVolume.classList.add("voice-volume");
      voiceVolume.min = 0;
      voiceVolume.max = 1;
      voiceVolume.step = 0.01;
      voiceVolume.value = initialState.selectedPart.drumRack.voicesVolume[voiceIndex];
      voiceVolume.addEventListener("change", updateStep);

      voice.appendChild(voiceVolume);
        
      for (var rowIndex = 0; rowIndex < numberOfRows; rowIndex++) {
        var row = document.createElement("p");
        row.id = `steps-row-${voiceIndex}-${rowIndex}`;
        row.classList.add("steps-row");
        
        voice.appendChild(row);
        
        for (var stepIndex = 0; stepIndex < numberOfStepsPerRow; stepIndex++) {
          const stepIndexInRow = (rowIndex * numberOfStepsPerRow) + stepIndex;
          if (stepIndexInRow < initialState.selectedPart.stepsLength) {
            var stepContainer = document.createElement("span");
            stepContainer.classList.add("step-container");
            row.appendChild(stepContainer);
            
            var input = document.createElement("input");
            input.type = "checkbox";
            input.classList.add("checkbox");
            input.classList.add("step");
            input.id = `step-${voiceIndex}-${rowIndex}-${stepIndexInRow}`;
            if (initialState.selectedPart.drumRack.steps[stepIndexInRow].sV[voiceIndex].st) {
              input.checked = true;
            }
            stepContainer.appendChild(input);
            input.addEventListener('click', updateStep);

            var speedInput = document.createElement("input");
            speedInput.type = "number";
            speedInput.step="0.001";
            speedInput.classList.add("number-input");
            speedInput.classList.add("step-speed");
            speedInput.value = initialState.selectedPart.drumRack.steps[stepIndexInRow].sV[voiceIndex].sp;
            stepContainer.appendChild(speedInput);
            speedInput.addEventListener("change", updateStep);
          }
        }
      }
    }
  }
  function onOpen(event) {
    console.log('Connection opened');
  }
  function onClose(event) {
    console.log('Connection closed');
    setTimeout(initWebSocket, 2000);
  }
  function onMessage(event) {
    console.log('onMessage event.data : ', event.data);
    initialState = JSON.parse(event.data);
    initCheckboxes();
    
    document.getElementById('part-id-span').textContent = initialState.selectedPartId;
    document.getElementById('tempo-slider-value').textContent = initialState.selectedPart.tempo;
    document.getElementById('tempo-slider').value = initialState.selectedPart.tempo;
  }
  function onLoad() {
    initWebSocket();
    initButtons();
    initCheckboxes();
  }
  function prevPart() {
    console.log('prevPart');
    websocket.send('prpt');
  }
  function nextPart() {
    console.log('nextPart');
    websocket.send('nxpt');
  }
  function play(){
    websocket.send('play');
    console.log('play');
  }
  function stop(){
    websocket.send('stop');
    console.log('stop');
  }
  function tempoChange() {
    // Get the slider element by its id
    console.log("tempo before : ", initialState.selectedPart.tempo);
    initialState.selectedPart.tempo = document.getElementById('tempo-slider').value;
    console.log("tempo after : ", initialState.selectedPart.tempo);
    
    websocket.send('upst'+ JSON.stringify(initialState));
    var tempoSliderValueElement = document.getElementById('tempo-slider-value');
    tempoSliderValueElement.textContent = event.target.value;
  }
  function updateStep() {
    var numberOfVoices = initialState.selectedPart.voicesNumber;
    var numberOfStepsPerRow= initialState.selectedPart.stepsLength;
    var numberOfRows = Math.ceil(initialState.selectedPart.stepsLength / numberOfStepsPerRow);

    // voicesVolume
    // voiceVolume.value = initialState.selectedPart.drumRack.voicesVolume[rowIndex];
    
    var elements = document.querySelectorAll('.step');
    var elementsSpeed = document.querySelectorAll('.step-speed');
    
    var elementsData = [];
    
    for (var i = 0; i < elements.length; i++) {
      if (elements[i].checked) {
        elementsData.push({
          id: elements[i].id,
          checked: elements[i].checked,
          speed: parseFloat(elementsSpeed[i].value)
        });
      }
    }

    var newSteps = Array.from({ length: numberOfRows * numberOfStepsPerRow }, () => ({sV: Array.from({ length: numberOfVoices }, () => ({st: 0, sp: 1.0}))}));
    elementsData.forEach((eld) => {
      var eldIdSplit = eld.id.split("-");
      var stepVoice = eldIdSplit[1];
      var stepIndex = eldIdSplit[eldIdSplit.length - 1];
      newSteps[stepIndex].sV[stepVoice].st = eld.checked;
      newSteps[stepIndex].sV[stepVoice].sp = eld.speed;
    });
    console.log("newSteps : ", newSteps);
    
    initialState.selectedPart.drumRack.steps = newSteps;
    console.log("new initial state : ", initialState);
    console.log("new initial state : ", JSON.stringify(initialState));
    websocket.send('upst'+ JSON.stringify(initialState));
  }

  function saveTextToFile(text, fileName) {
    const blob = new Blob([text], { type: 'text/plain' });
    const url = URL.createObjectURL(blob);
    
    const a = document.createElement('a');
    a.href = url;
    a.download = fileName;
    
    document.body.appendChild(a);
    a.click();
    
    // Clean up
    document.body.removeChild(a);
    URL.revokeObjectURL(url);
  }
  
  function setup() {
    console.log('setup');
  }
  
  function draw() {
    console.log('draw');
  }

</script>
</body>
</html>
)rawliteral";

#endif // WEBAPP_H
