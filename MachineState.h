#ifndef MACHINESTATE_H
#define MACHINESTATE_H

#include <string> // Include the string header for std::string
#include <vector>

#include "MachineConfig.h"
#include "Part.h"

class MachineState {
public:
    // MachineState(const std::string& description);
    MachineState();

    // std::string title;
    MachineConfig machineConfig; // Configuration object
    int selectedPartId;
    std::vector<Part> parts;

    void updateState(std::string newState);
    std::string toJsonString();

private:
    // std::string title;
};

#endif // MACHINESTATE_H
