class MachineState {
  machineConfig: MachineConfig;
  selectedPart: Part;

  constructor() {
    this.machineConfig = new MachineConfig();
    this.selectedPart = new Part();
  }
}

class MachineConfig {
  theme: ThemeConfig;
  fileSystem: FileSystemConfig;

  constructor() {
    this.theme = new ThemeConfig();
    this.fileSystem 
  }
}

class ThemeConfig {
  lightsColor: LigthColor;
  constructor() {}
}

class FileSystemConfig {
  exportsFolderPath: "songs";
  partsFolderPath: "parts";
  samplesFolderPath: "samples";
  constructor() {}
}

enum LigthColor {
  RED = "#FF0000",
  ORANGE = "#FF7F00",
  YELLOW = "#FFFF00",
  GREEN = "#00FF00",
  AZUR = "#007FFF",
  BLUE = "#0000FF",
  VIOLET = "#7F00FF",
}

class Song {
  parts: Part[];
  constructor() {}
}

class Part {
  name: string;
  // signature: PartSignature;
  length: number;

  drumRack: DrumRack;
  sampler1: Sampler;
  sampler2: Sampler;
  strech: Strech;
  constructor() {}
}

// enum PartSignature {
// }

class Track {
  type: TrackType;
  constructor() {}
}

type TrackType = "DrumRack" | "Sampler" | "Strech";

class DrumRack extends Track {
  constructor() {
    super();
  }
}

class Sampler extends Track {
  constructor() {
    super();
  }
}

class Strech extends Track {
  constructor() {
    super();
  }
}

class Sample {
  constructor() {}
}
