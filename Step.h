#ifndef STEP_H
#define STEP_H

#include <iostream>
#include <vector>
#include <string>

class StepVoice {
public:
  StepVoice(int statusParameter = 0, float speedParameter = 1.0);

  int st;
  float sp;

  std::string toJsonString();
private:
};

class Step {
public:
    Step(int voicesNumberParameter);
    
    std::vector<StepVoice> sV;

    std::string toJsonString();
private:
};

#endif // STEP_H
